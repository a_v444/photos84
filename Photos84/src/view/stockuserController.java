package view;

import java.io.IOException;

import Model.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
/**
 * This class houses a controller to run a GUI for the Stock User where the user can view, create and 
 * delete <code>Photo</code> objects in the workspace.
 * @author Cole Stevens
 * @author Anuj Venkatesan
 * @version 1.0
 * @see Model.Photo#Photo(String)
 */
public class stockuserController {

	Stage stage;
	@FXML Button logout;
	@FXML Button close;
	/**
	 * This method takes in an ActionEvent as a parameter and calls a method to perform an action based on
	 * the parameter. Based on the button clicked, the {@link #logout(Stage)} method will be called or the 
	 * program will end.
	 * @param e the ActionEvent triggered by clicking a button on the GUI.
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @see #logout
	 */
	public void convert(ActionEvent e) throws IOException {
		Button b = (Button)e.getSource();
		if(b == logout)
		{

			logout(stage);
		
		}
		else
		{
			
			System.exit(0);
		}
	
	}
	/**
	 * This method takes a Stage variable as a parameter and sets it as the <code>stage</code> variable
	 * saved locally to the controller.
	 * @param m the Stage to be saved locally to the Controller.
	 */
	public void setStage(Stage m)
	{
		stage = m;
	}
	/**
	 * This method takes a Stage variable as a parameter and creates a new Stage as a GridPane and passes
	 * the parameter to be the newly created GridPane. This GridPane corresponds to the loginpage.fxml which
	 * will launch the GUI for the Login page.
	 * @param primaryStage the Stage to be set as the newly created GridPane
	 * @throws IOException
	 * @see {@link #setStage(Stage)}
	 */
	private void logout(Stage primaryStage) throws IOException
	{
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/loginpage.fxml"));
		GridPane root = (GridPane)loader.load();
		Scene scene = new Scene(root);
		loginpageController a = loader.getController();
		a.setStage(primaryStage);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Photos");
		primaryStage.setResizable(false);
		primaryStage.show();
	}
	
}
