package view;

import java.io.*;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Tooltip;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import Model.*;
import java.util.*;

/**
 * This class houses a controller to launch a GUI that allows the user to make changes to a 
 * <code>Album</code> object.
 * @author Cole Stevens
 * @author Anuj Venkatesan
 * @version 1.0
 * @see Model.Album#Album(String)
 */
public class AlbumController {

	Stage stage;
    @FXML
    private Button addphoto;
    @FXML
    private TextField file;

    @FXML
    private Button addtag;

    @FXML
    private Button back;

    @FXML
    private Button caption;

    @FXML
    private Button close;

    @FXML
    private Button copy;

    @FXML
    private Button deletephoto;

    @FXML
    private Button deletetag;

    @FXML
    private Button displayphoto;

    @FXML
    private Button logout;

    @FXML
    private Button move;
    @FXML 
    private ListView<String> captionlist;
    private ObservableList<String> captions;
    @FXML
    private ListView<ImageView> photolist;

    @FXML
    private Button slide;
    private ObservableList<ImageView> obsList;   
    User user;
    Album album;
   
    /**
     * This method is called when the controller is called. Initializes two observableArrayLists of Photos
     * and captions to be those contained in the <code>Album</code> object.
     * @param stage the Stage that houses the GUI
     * @throws ClassNotFoundException
     * @throws IOException
	 * @see Model.Album#getphotos()
	 * @see Model.Photo#getfile()
	 * @see Model.Photo#getcaption()
     */
    public void start(Stage stage) throws ClassNotFoundException, IOException
	{
		
		obsList = FXCollections.observableArrayList();
		captions = FXCollections.observableArrayList();
		
		for(int i =0; i < album.getphotos().size();i++)
		{
			 InputStream stream = new FileInputStream(album.getphotos().get(i).getfile());
			 Image image = new Image(stream);
			 ImageView imageview = new ImageView();
			 imageview.setImage(image);
			 imageview.setFitWidth(125);
			 imageview.setFitHeight(25);
			
			
			 
			 obsList.add(imageview);
			 captions.add(album.getphotos().get(i).getcaption());
			 
		}
		captionlist.setItems(captions);
		photolist.setItems(obsList);
		
		
	}
    /**
     * This method takes in an ActionEvent as a parameter and calls another method based on the button that
     * was pressed in the GUI. 
     * @param event
     * @throws IOException
     * @throws ClassNotFoundException
     * @see #logout
     * @see #slide
     * @see #back
     */
    @FXML
   public  void convert(ActionEvent event) throws IOException, ClassNotFoundException {
    	Button b = (Button)event.getSource();
    	if(b == logout)
    	{
    		logout(stage);
    	}
    	if(b== close)
    	{
    		System.exit(0);
    	}
    	if(b == back)
    	{
    		back(stage);
    	}
    	if(b == slide)
    	{
    		if(album.getphotos().size() != 0)
    		{
    		slide(stage);
    		}
    	}
    }
    /**
     * This method takes in a Stage as an argument and creates a new GridPane as a Stage which launches a 
     * slideshow GUI. Passes the <code>User</code> and <code>Album</code> objects to the next stage.
     * @param primaryStage the current Stage the GUI is housed in
     * @throws IOException
     * @see {@link #setStage(Stage)}
     * @see Model.User#User(String)
     * @see Model.Album#Album(String)
     */
    private void slide(Stage primaryStage) throws IOException
    {
    	FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/slideshow.fxml"));
		GridPane root = (GridPane)loader.load();
		Scene scene = new Scene(root);
		slideshowController a = loader.getController();
		a.setStage(stage);
		a.user = user;
		a.album = album;
		a.index = 0;
		a.start(primaryStage);
		
		primaryStage.setScene(scene);
		primaryStage.setTitle("Photos");
		primaryStage.setResizable(false);
		primaryStage.show();
    }
    /**
     * This method takes in a Stage variable as a parameter and passes it to a newly created GridPane stage
     * which launches the User GUI. Passes the <code>User</code> object to the stage.
     * @param primaryStage
     * @throws IOException
     * @throws ClassNotFoundException
     * @see #setStage(Stage)
     * @see Model.User#User(String)
     */
    private void back(Stage primaryStage) throws IOException, ClassNotFoundException
    {
    	FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/user.fxml"));
		GridPane root = (GridPane)loader.load();
		Scene scene = new Scene(root);
		UserController a = loader.getController();
		a.setStage(primaryStage);
		a.user = user;
		a.start(primaryStage);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Photos");
		primaryStage.setResizable(false);
		primaryStage.show();
    }
    /**
     * This method takes an ActionEvent as an argument and displays an Image as a new Scene along with an
     * Alert with photo information.
     * @param event ActionEvent that the user triggered by clicking a button.
     * @throws FileNotFoundException
     * @see Model.Album#getphotos()
	 * @see Model.Photo#getfile()
     */
    @FXML
    public void display(ActionEvent event) throws FileNotFoundException {
    	String file = album.getphotos().get(photolist.getSelectionModel().getSelectedIndex()).getfile();
    	InputStream stream = new FileInputStream(file);
        Image image = new Image(stream);
        //Creating the image view
        ImageView imageView = new ImageView();
        //Setting image to the image view
        imageView.setImage(image);
        imageView.setFitWidth(300);
        imageView.setFitHeight(300);
        //Setting the Scene object
        Group root = new Group(imageView);
        Stage newstage = new Stage();
        Scene scene = new Scene(root, 300, 300);
        newstage.setTitle(album.getphotos().get(photolist.getSelectionModel().getSelectedIndex()).getcaption());
        newstage.setScene(scene);
        newstage.show();
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setHeaderText("Photo information");
        Photo photo = album.getphotos().get(photolist.getSelectionModel().getSelectedIndex());
        alert.setContentText(photo.toString());
        alert.show();
        
    }
    /**
     * This method triggers on an ActionEvent which is the parameter. Based on the button pressed by the 
     * user, a photo will be added or deleted to and from an album, a caption will be added, a tag will be
     * added or deleted, or the start method will be called.
     * @param event the ActionEven triggered by the user when they clicked a button
     * @throws ClassNotFoundException
     * @throws IOException
     * @see Model.Album#getphotos()
     * @see Model.Photo#setDate()
     * @see Model.Album#addPhoto(Photo)
     * @see Model.userslist#read()
     * @see Model.userslist#userslist()
     * @see Model.userslist#getuser(String)
     * @see Model.userslist#writeObject(userslist)
     * @see Model.User#getAlbum(String)
     * @see Model.Album#getname()
     * @see Model.Album#deletePhoto(Photo)
     * @see Model.Photo#setcaption(String)
     * @see Model.Photo#addtag(Tag)
     * @see Model.Tag#Tag(String)
     * @see Model.Photo#deletetag(Tag)
     * @see Model.Tag#addvalue(String)
     */
    @FXML
   public  void update(ActionEvent event) throws ClassNotFoundException, IOException {
    	Button b = (Button)event.getSource();
    	if(b == addphoto)
    	{
    		File f = new File(file.getText());
    		if(!f.exists())
    		{
    			 Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Alert");
					alert.setContentText("Photo does not exist");
					alert.showAndWait();
    		}
    		else if(album.getphotos().contains(new Photo(file.getText())))
    		{
    			Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Alert");
				alert.setContentText("Photo already in album");
				alert.showAndWait();
    		}
    		else
    		{
    			Photo a = new Photo(file.getText());
    			a.setDate();
    			
    		album.addPhoto(a);
    		userslist store = userslist.read();
    		store.getuser(user.getname()).getAlbum(album.getname()).addPhoto(a);
    		userslist.writeObject(store);
    		}
    	}
    	if(b == deletephoto)
    	{
    		
    		if(!album.getphotos().isEmpty())
    		{
    		int index = photolist.getSelectionModel().getSelectedIndex();
    		Photo d = album.getphotos().get(index);
    		album.deletePhoto(d);
    		userslist store = userslist.read();
    		store.getuser(user.getname()).getAlbum(album.getname()).deletePhoto(d);
    		userslist.writeObject(store);
    		}
    	}
    	if(b == caption)
    	{
    		
    		int index = photolist.getSelectionModel().getSelectedIndex();

    		TextInputDialog dialog = new TextInputDialog();
    		dialog.initOwner(stage); dialog.setTitle("Set Caption");
    		dialog.setHeaderText("Enter Caption");
    		

    		Optional<String> result = dialog.showAndWait();
    		if (result.isPresent()) 
    		{ 
    			album.getphotos().get(index).setcaption(result.get());
    		
    		
    			userslist store = userslist.read();
    			
    			for(int i =0; i < user.getalbums().size();i++)
    			{
    				Photo select = album.getphotos().get(index);
    				if(user.getalbums().get(i).getphotos().contains(select))
    				{
    					user.getalbums().get(i).getPhoto(select).setcaption(result.get());
    					store.getuser(user.getname()).getalbums().get(i).getPhoto(select).setcaption(result.get());
    				
    				}
    			}
        		store.getuser(user.getname()).getAlbum(album.getname()).getphotos().get(photolist.getSelectionModel().getSelectedIndex()).setcaption(result.get());
        		
        		userslist.writeObject(store);
    		}
    		
    	}
    	if(b== addtag)
    	{
    		int index = photolist.getSelectionModel().getSelectedIndex();
    	
    		TextInputDialog dialog = new TextInputDialog();
    		dialog.initOwner(stage); 
    		dialog.setTitle("Add tag");
    		dialog.setHeaderText("Format: type,value \nbuilt in tags: Location, Person \nEx: Location,New York");
    		Optional<String> result = dialog.showAndWait();
    		if(result.isPresent())
    		{
    			StringTokenizer a = new StringTokenizer(result.get(),",");
    			ArrayList<String> strings = new ArrayList<String>();
    			while(a.hasMoreTokens())
    			{
    				strings.add(a.nextToken());
    			}
    			if(!(strings.size() == 2))
    			{
    				 Alert alert = new Alert(AlertType.INFORMATION);
 					alert.setTitle("Alert");
 					alert.setContentText("invalid input");
 					alert.show();
    			}
    			else
    			{
    				String type = strings.get(0).toLowerCase();
    			
    				String value = strings.get(1).toLowerCase();
    				if(album.getphotos().get(index).gettags().contains(new Tag(type)))
    				{
    					if(album.getphotos().get(index).gettag(type).getvalues().contains(value))
    					{
    						 Alert alert = new Alert(AlertType.INFORMATION);
    		 					alert.setTitle("Alert");
    		 					alert.setContentText("tag type value pair already exists");
    		 					alert.show();
    					}
    					else
    					{
    						
    						
    						
    						userslist store = userslist.read();
    						for(int i =0; i < user.getalbums().size();i++)
    		    			{
    		    				Photo select = album.getphotos().get(index);
    		    				if(user.getalbums().get(i).getphotos().contains(select))
    		    				{
    		    					user.getalbums().get(i).getPhoto(select).gettag(type).addvalue(value);
    		    					store.getuser(user.getname()).getalbums().get(i).getPhoto(select).gettag(type).addvalue(value);
    		    				
    		    				}
    		    			}
    		        	
    		        		userslist.writeObject(store);
    						
    					}
    				}
    				else
    				{
    					Tag t = new Tag(type);
						t.addvalue(value);
					
						userslist store = userslist.read();
						for(int i =0; i < user.getalbums().size();i++)
		    			{
		    				Photo select = album.getphotos().get(index);
		    				if(user.getalbums().get(i).getphotos().contains(select))
		    				{
		    					user.getalbums().get(i).getPhoto(select).addtag(t);
		    					store.getuser(user.getname()).getalbums().get(i).getPhoto(select).addtag(t);
		    				
		    				}
		    			}
		        		
		        		userslist.writeObject(store);
    					
    				}
    				
    			}
    		}
    		
    	}
    	if(b== deletetag)
    	{
    		int index = photolist.getSelectionModel().getSelectedIndex();

    		TextInputDialog dialog = new TextInputDialog();
    		dialog.initOwner(stage); 
    		dialog.setTitle("Delete tag");
    		dialog.setHeaderText("Format: type,value \ntype in tag pair to delete specific value \ntype in only the type to delete entire tag");
    		Optional<String> result = dialog.showAndWait();
    		if(result.isPresent())
    		{
    			StringTokenizer a = new StringTokenizer(result.get(),",");
    			ArrayList<String> strings = new ArrayList<String>();
    			while(a.hasMoreTokens())
    			{
    				strings.add(a.nextToken().toLowerCase());
    			}
    			if(strings.size() == 1)
    			{
    			
    				
    				userslist store = userslist.read();
    				for(int i =0; i < user.getalbums().size();i++)
	    			{
	    				Photo select = album.getphotos().get(index);
	    				if(user.getalbums().get(i).getphotos().contains(select))
	    				{
	    					user.getalbums().get(i).getPhoto(select).deletetag(new Tag(strings.get(0)));
	    					store.getuser(user.getname()).getalbums().get(i).getPhoto(select).deletetag(new Tag(strings.get(0)));
	    				
	    				}
	    			}
	        	
	        		
	        		userslist.writeObject(store);
	        		
    			}
    			else if(strings.size() ==2)
    			{
    				if(!album.getphotos().get(index).gettags().contains(new Tag(strings.get(0))))
    				{
    					Alert alert = new Alert(AlertType.INFORMATION);
     					alert.setTitle("Alert");
     					alert.setContentText("tag does not exist");
     					alert.show();
    				}
    				else
    				{
    			
    				userslist store = userslist.read();
    				for(int i =0; i < user.getalbums().size();i++)
	    			{
	    				Photo select = album.getphotos().get(index);
	    				if(user.getalbums().get(i).getphotos().contains(select))
	    				{
	    					user.getalbums().get(i).getPhoto(select).gettag(strings.get(0)).removevalue(strings.get(1));
	    					store.getuser(user.getname()).getalbums().get(i).getPhoto(select).gettag(strings.get(0)).removevalue(strings.get(1));
	    	        	
	    				
	    				}
	    			}
	        		
	        		userslist.writeObject(store);
    				}
    			}
    			else
    			{
    				Alert alert = new Alert(AlertType.INFORMATION);
 					alert.setTitle("Alert");
 					alert.setContentText("invalid input");
 					alert.show();
    			}
    			
    		}
    	}
    	start(stage);

    }
    /**
     * This method takes in an ActionEvent as a parameter. If the button clicked corresponds to the requested
     * action by the user. The method will move a <code>Photo</code> object to a different <code>Album</code>
     * object.
     * @param event the ActionEvent triggered by the user when they clicked a button
     * @throws ClassNotFoundException
     * @throws IOException
     * @see Model.userslist#userslist()
     * @see Model.Photo#Photo(String)
     * @see Model.Album#Album(String)
     * @see Model.User#getAlbum(String)
     * @see Model.User#User(String)
     * @see Model.Album#getphotos()
     * @see Model.userslist#writeObject(userslist)
     */
    public void move(ActionEvent event) throws ClassNotFoundException, IOException
    {
    	Button b = (Button)event.getSource();
    	userslist store = userslist.read();
    	int index = photolist.getSelectionModel().getSelectedIndex();
    	Photo d = album.getphotos().get(index);
    	if(b== move)
    	{
    		TextInputDialog dialog = new TextInputDialog();
    		dialog.initOwner(stage); 
    		dialog.setTitle("Move Photo");
    		dialog.setHeaderText("Enter Album of destination");
    		Optional<String> result = dialog.showAndWait();
    		if(result.isPresent())
    		{
    			
    		if(user.getalbums().contains(new Album(result.get()))&& !(user.getAlbum(result.get()).getphotos().contains(d)))
    		{
    	
    	
    		album.deletePhoto(d);
    		store.getuser(user.getname()).getAlbum(album.getname()).deletePhoto(d);
    		user.getAlbum(result.get()).addPhoto(d);
    		store.getuser(user.getname()).getAlbum(result.get()).addPhoto(d);
    		
    		}
    		else
    		{
    			Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Alert");
					alert.setContentText("Album does not exist or photo already exists in album");
					alert.show();
    		}
    	}
    	}
    	else
    	{
    		TextInputDialog dialog = new TextInputDialog();
    		dialog.initOwner(stage); 
    		dialog.setTitle("Copy Photo");
    		dialog.setHeaderText("Enter Album name to copy to");
    		Optional<String> result = dialog.showAndWait();
    		
    		if(result.isPresent())
    		{
    			if(user.getalbums().contains(new Album(result.get())) && !(user.getAlbum(result.get()).getphotos().contains(d)))
        		{
    				user.getAlbum(result.get()).addPhoto(d);
    				store.getuser(user.getname()).getAlbum(result.get()).addPhoto(d);
        		}
    		}
    		else
    		{
    			Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Alert");
					alert.setContentText("Album does not exist or photo already exists in album");
					alert.show();
    		}
    	}
    	userslist.writeObject(store);
    	start(stage);
    }
    /**
     * This method takes in a Stage variable as a parameter. Sets the parameter equal to a new Stage 
     * variable, which is a GridPane controlled by the loginpage Controller.
     * @param primaryStage the stage to be set equal to the new stage.
     * @throws IOException
     */
    private void logout(Stage primaryStage) throws IOException 
	{
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/loginpage.fxml"));
		GridPane root = (GridPane)loader.load();
		Scene scene = new Scene(root);
		loginpageController a = loader.getController();
		a.setStage(primaryStage);
		
		primaryStage.setScene(scene);
		primaryStage.setTitle("Photos");
		primaryStage.setResizable(false);
		primaryStage.show();
	}
    /**
	 * This method takes a Stage variable as a parameter and sets it as the <code>stage</code> variable
	 * saved locally to the controller.
	 * @param m the Stage to be saved locally to the Controller.
	 */
	public void setStage(Stage primaryStage) {
		stage= primaryStage;
		
	}
}