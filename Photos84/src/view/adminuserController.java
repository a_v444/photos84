package view;

import java.io.IOException;
import Model.User;
import Model.userslist;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
/**
 * This class houses a controller to run a GUI for an Admin User Sub-System where the user can view, create and 
 * delete <code>User</code> objects saved to a <code>userslist</code> object in a local file.
 * @author Cole Stevens
 * @author Anuj Venkatesan
 * @version 1.0
 * @see Model.userslist#userslist()
 * @see Model.User#User(String)
 */
public class adminuserController {
	
	Stage stage;
	@FXML Button list;
	@FXML Button create;
	@FXML Button delete;
	@FXML Button logout;
	@FXML TextField create1;
	@FXML TextField delete1;
	
	@FXML
	private ListView<User> ListView;
	private ObservableList<User> obsList;
	/**
	 * This method takes in an ActionEvent as a parameter and calls a method to perform an action based on
	 * the parameter. Can create and delete a user by reading a Textfield from the GUI. Otherwise, it will
	 * call the start method and relaunch the GUI.
	 * @param e the ActionEvent triggered by clicking a button on the GUI.
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @see #logout
	 * @see Model.userslist#adduser(User)
	 * @see Model.userslist#read()
	 * @see Model.userslist#deleteuser(User)
	 * @see Model.userslist#getuser(String)
	 * @see Model.userslist#userslist()
	 * @see Model.User#User(String)
	 * @see #start(Stage)
	 */
	public void convert(ActionEvent e) throws IOException, ClassNotFoundException {
		Button b = (Button)e.getSource();
		if(b == logout) 
		{
			logout(stage);
		}
		
		else if (b == create)
		{
			String name = create1.getText();
			User usr = new User(name);
			
			userslist a = userslist.read();
			if(a.getusers().contains(usr))
			{
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Alert");
				alert.setContentText("User already exists");
				alert.showAndWait();
			}
			a.adduser(usr);
			
		} 
		else
		{
		
			String name = delete1.getText();
			userslist a = userslist.read();
			if(a.getusers().contains(new User(name)))
			{
			User usr = new User(a.getuser(name).toString());
			
			a.deleteuser(usr);
			}
			else
			{
				 Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Alert");
					alert.setContentText("User does not exist");
					alert.showAndWait();
			}
			
		} 
		start(stage);
	}
	/**
	 * This method takes a Stage variable as a parameter and sets it as the <code>stage</code> variable
	 * saved locally to the controller.
	 * @param m the Stage to be saved locally to the Controller.
	 */
	public void setStage(Stage m) {
		stage = m;
	}
	/**
	 * This method takes a Stage variable as a parameter and creates a new Stage as a GridPane and passes
	 * the parameter to be the newly created GridPane. This GridPane corresponds to the loginpage.fxml which
	 * will launch the GUI for the Login page.
	 * @param primaryStage the Stage to be set as the newly created GridPane
	 * @throws IOException
	 * @see {@link #setStage(Stage)}
	 */
	private void logout(Stage primaryStage) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/loginpage.fxml"));
		GridPane root = (GridPane)loader.load();
		Scene scene = new Scene(root);
		loginpageController a = loader.getController();
		a.setStage(primaryStage);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Photos");
		primaryStage.setResizable(false);
		primaryStage.show();
	}
	/**
	 * This method is called first when the Controller is called as the new Stage. Reads the file where the
	 * list of Users is saved locally and launches a ListView of the <code>userslist</code>. 
	 * @param primaryStage The Stage to be passed when the ListView is called.
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @see Model.userslist#read()
	 * @see Model.userslist#getusers()
	 */
	public void start(Stage primaryStage) throws IOException, ClassNotFoundException {
		userslist a = userslist.read();
		obsList = FXCollections.observableArrayList(a.getusers());
		ListView.setItems(obsList);
	}
}
