package view;

import java.io.FileInputStream;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;
import java.util.StringTokenizer;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import Model.*;
/**
 * This class houses a controller to run a GUI for a User Sub-System where the user can view, 
 * create and delete <code>Album</code> objects saved to a <code>User</code> object in a local file.
 * @author Cole Stevens
 * @author Anuj Venkatesan
 * @version 1.0
 * @see Model.Album#Album(String)
 * @see Model.User#User(String)
 */
public class UserController {

	Stage stage;
	@FXML Button logout;
	@FXML Button close;
	@FXML Button create;
	@FXML Button delete;
	@FXML Button open;
	@FXML ListView<Album> albumlist;
	@FXML TextField createalbum;
	@FXML Button searchdate;
	@FXML Button searchtag;
	@FXML Button rename;
	@FXML Button createsearch;
	@FXML ListView<ImageView> searchresults;
	private ObservableList<ImageView> searchList;
	 ArrayList<Photo> searchalbum;
	private ObservableList<Album> obsList;   
	User user;
	/**
	 * This method is called first when the Controller is called as the new Stage. Launches a ListView of 
	 * the <code>Album</code> objects the <code>User</code> object has saved to it. 
	 * @param primaryStage The Stage to be passed when the ListView is called.
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @see Model.User#getalbums()
	 */
	public void start(Stage stage) throws ClassNotFoundException, IOException
	{
		obsList = FXCollections.observableArrayList(user.getalbums());
		albumlist.setItems(obsList);
		
	}
	  @FXML
	  /**
	   * This method has a ActionEvent variable as its parameter. Triggers a TextInputDialog that the user
	   * has to enter in order to name the album. Afterwards, if the name is not already being used, the
	   * method will populate the new album with the search photos.
	   * @param event the ActionEvent triggered by the user when they clicked the button.
	   * @throws ClassNotFoundException
	   * @throws IOException
	   */
	   public void createsearchalbum(ActionEvent event) throws ClassNotFoundException, IOException {

			 TextInputDialog dialog = new TextInputDialog();
	    		dialog.initOwner(stage); 
	    		dialog.setTitle("Create Album of search results");
	    		dialog.setHeaderText("Enter name of album");
	    		
	    		Optional<String> result = dialog.showAndWait();
	    		
	    		if(result.isPresent())
	    		{
	    			if(user.getalbums().contains(new Album(result.get())))
	    			{
	    				Alert alert = new Alert(AlertType.INFORMATION);
						alert.setTitle("Alert");
						alert.setContentText("Album already exists");
						alert.show();
	    			}
	    			else
	    			{
	    				Album newalbum = new Album(result.get());
	    				for(Photo a: searchalbum)
	    				{
	    					newalbum.addPhoto(a);
	    				}
	    				user.addAlbum(newalbum);
	    			}
	    		}
	    		start(stage);

	    }
	  /**
	   * his method has a ActionEvent variable as its parameter. Creates a blank ArrayList of the type
	   * <code>Photo</code>. Based on the button pressed, the method will search all the user's 
	   * <code>Album</code> objects for <code>Tag</code> or <code>Date</code> objects that match their
	   * search and take the <code>Photo</code> objects of the matching criteria and pass it to the 
	   * {@link #createsearchalbum(ActionEvent)} method.
	   * @param e the ActionEvent triggered by the user when they clicked the button.
	   * @throws FileNotFoundException
	   * @throws ParseException
	   * @see Model.Photo#Photo(String)
	   * @see Model.Album#Album(String)
	   * @see Model.Tag#Tag(String)
	   * @see #createsearchalbum(ActionEvent)
	   * @see Model.User#getalbums()
	   * @see Model.Album#getphotos()
	   * @see Model.Photo#gettag(String)
	   * @see Model.Tag#getvalues()
	   */
	public void search(ActionEvent e) throws FileNotFoundException, ParseException
	{
		searchalbum = new ArrayList<Photo>();
		Button b = (Button)e.getSource();
		if(b == searchtag)
		{
			 TextInputDialog dialog = new TextInputDialog();
	    		dialog.initOwner(stage); 
	    		dialog.setTitle("Search by tag");
	    		dialog.setHeaderText("Enter tag type, tag value followed by And or OR and another tag if search involes 2 tags "
	    				+ "\nEx: person,child"
	    				+ "\nEx: person,child,OR,location,new york");
	    		
	    		Optional<String> result = dialog.showAndWait();
	    		
	    		if(result.isPresent())
	    		{
	    			StringTokenizer tokens = new StringTokenizer(result.get(),",");
	    			ArrayList<String> strings = new ArrayList<String>();
	    			while(tokens.hasMoreTokens())
	    			{
	    				strings.add(tokens.nextToken().toLowerCase());
	    			}
	    		
	    			if(strings.size() != 5 && strings.size() !=2)
	    			{
	    				Alert alert = new Alert(AlertType.INFORMATION);
						alert.setTitle("Alert");
						alert.setContentText("invalid input");
						alert.show();
	    			}
	    			else
	    			{
	    				
	    					for(int i =0; i < user.getalbums().size();i++)
	    					{
	    						for(int j =0; j < user.getalbums().get(i).getphotos().size();j++)
	    						{
	    							Photo current = user.getalbums().get(i).getphotos().get(j);
	    							Tag one = current.gettag(strings.get(0));
	    							if(!searchalbum.contains(current))
	    							{
	    							if(strings.size() == 5)
	    							{
	    								
	    								Tag two = current.gettag(strings.get(3));
	    								if(strings.get(2).equalsIgnoreCase("OR"))
	    								{
	    									if(one != null && one.getvalues().contains(strings.get(1)))
	    									{
	    										searchalbum.add(current);
	    									}
	    									else if(two != null && two.getvalues().contains(strings.get(4)))
	    									{
	    										searchalbum.add(current);
	    									}
	    								}
	    								else
	    								{
	    									if(one != null && one.getvalues().contains(strings.get(1)) && two != null && two.getvalues().contains(strings.get(4)))
	    									{
	    										searchalbum.add(current);
	    									}
	    								}
	    							}
	    							else
	    							{
	    								if(one != null && one.getvalues().contains(strings.get(1)))
	    								{
	    									searchalbum.add(current);
	    								}
	    							}
	    						}
	    						}
	    					}
	    			
		}
	    		}
		
	}
		else
		{
			 TextInputDialog dialog = new TextInputDialog();
	    		dialog.initOwner(stage); 
	    		dialog.setTitle("Search by Date");
	    		dialog.setHeaderText("Enter the endpoint dates of the range in format MM/DD/YYYY,MM/DD/YYYY"
	    				+ "\nEx: 10/28/2021,11/14/2021");
	    		
	    		Optional<String> result = dialog.showAndWait();
	    		
	    		if(result.isPresent())
	    		{
	    			StringTokenizer tokens = new StringTokenizer(result.get(),",");
	    			ArrayList<String> strings = new ArrayList<String>();
	    			while(tokens.hasMoreTokens())
	    			{
	    				strings.add(tokens.nextToken().toLowerCase());
	    			}
	    			if(strings.size() != 2 || strings.get(0).length() != 10 || strings.get(1).length() != 10)
	    			{
	    				Alert alert = new Alert(AlertType.INFORMATION);
						alert.setTitle("Alert");
						alert.setContentText("invalid input");
						alert.show();
	    			}
	    			else
	    			{
	    			Date start = new SimpleDateFormat("MM/dd/yyyy").parse(strings.get(0));
	    			Date end = new SimpleDateFormat("MM/dd/yyyy").parse(strings.get(1));
	    			Calendar time = Calendar.getInstance();
	    			time.setTime(end);
	    			time.set(Calendar.HOUR_OF_DAY,23);
	    			time.set(Calendar.MINUTE, 59);
	    			time.set(Calendar.SECOND, 59);
	    			end = time.getTime();
	    			for(int i =0; i < user.getalbums().size();i++)
					{
						for(int j =0; j < user.getalbums().get(i).getphotos().size();j++)
						{
							Photo current = user.getalbums().get(i).getphotos().get(j);
							
							if(!start.after(current.getdate()) && !end.before(current.getdate()))
							{
								if(!searchalbum.contains(current))
								{
								searchalbum.add(current);
								}
							}
						}
					}
	    			
	    			}
	    		}
		}

	searchList = FXCollections.observableArrayList();
	for(int x = 0; x < searchalbum.size(); x++)
	{
		InputStream stream = new FileInputStream(searchalbum.get(x).getfile());
		 Image image = new Image(stream);
		 ImageView imageview = new ImageView();
		 imageview.setImage(image);
		 imageview.setFitWidth(125);
		 imageview.setFitHeight(25);
		 searchList.add(imageview);
		 
		
	}
	searchresults.setItems(searchList);	
	}
	/**
	 * This method takes in an ActionEvent as a parameter and calls a method to perform an action based on
	 * the parameter. Based on the button clicked, the {@link #logout(Stage)} method will be called, the 
	 * method {@link #openalbum(Stage)} will be called, or the program will end.
	 * @param e the ActionEvent triggered by clicking a button on the GUI.
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @see #logout
	 * @see #openalbum(Stage)
	 */
	public void convert(ActionEvent e) throws IOException, ClassNotFoundException {
		Button b = (Button)e.getSource();
		if(b == logout)
		{

			logout(stage);
		
		}
		else if(b == open)
		{
			openalbum(stage);
		}
		else
		{
			
			System.exit(0);
		}
	
	}
	/**
	 * This method takes in a Stage variable as the only paramater. Creates a new Stage of the scene type 
	 * GridPane and loads it with the AlbumController. Passes the <code>User</code> to the new Stage
	 * as well.
	 * @param e the Stage variable that houses the current GUI.
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private void openalbum(Stage primaryStage) throws IOException, ClassNotFoundException {
	
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/album.fxml"));
		GridPane root = (GridPane)loader.load();
		Scene scene = new Scene(root);
		AlbumController a = loader.getController();
		a.setStage(primaryStage);
		a.user = user;
	
		a.album = albumlist.getSelectionModel().getSelectedItem();
		a.start(primaryStage);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Photos");
		primaryStage.setResizable(false);
		primaryStage.show();
	}
	/**
	 * This method takes a Stage variable as a parameter and sets it as the <code>stage</code> variable
	 * saved locally to the controller.
	 * @param m the Stage to be saved locally to the Controller.
	 */
	public void setStage(Stage m)
	{
		stage = m;
	}
	/**
	 * This method takes a Stage variable as a parameter and creates a new Stage as a GridPane and passes
	 * the parameter to be the newly created GridPane. This GridPane corresponds to the loginpage.fxml which
	 * will launch the GUI for the Login page.
	 * @param primaryStage the Stage to be set as the newly created GridPane
	 * @throws IOException
	 * @see {@link #setStage(Stage)}
	 */
	private void logout(Stage primaryStage) throws IOException
	{
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/loginpage.fxml"));
		GridPane root = (GridPane)loader.load();
		Scene scene = new Scene(root);
		loginpageController a = loader.getController();
		a.setStage(primaryStage);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Photos");
		primaryStage.setResizable(false);
		primaryStage.show();
	}
	/**
	 * This method takes in an ActionEvent as a parameter. Based on the button the user pressed, the method
	 * will either create or delete an album from the <code>User</code> object.
	 * @param event the ActionEvent triggered by the user when they press a button
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @see Model.User#User(String)
	 * @see Model.User#getalbums()
	 * @see Model.User#addAlbum(Album)
	 * @see Model.User#deleteAlbum(Album)
	 * @see Model.userslist#getuser(String)
	 * @see Model.Album#setname(String)
	 * @see Model.Album#getname()
	 * @see Model.userslist#userslist()
	 * @see Model.userslist#read()
	 * @see Model.userslist#writeObject(userslist)
	 */
	 public void update(ActionEvent event) throws ClassNotFoundException, IOException {
		 Button b = (Button)event.getSource();
		 if(b == create)
		 {
			 if(user.getalbums().contains(new Album(createalbum.getText())))
			 {
				 Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Alert");
					alert.setContentText("Album of same name already exists");
					alert.showAndWait();
			 }
			 else
			 {
			 user.addAlbum(new Album(createalbum.getText()));
			 }
			
		 }
		 if(b== delete)
		 {
			 user.deleteAlbum(albumlist.getSelectionModel().getSelectedItem());
			 
		 }
		 if(b == rename)
		 {
			 TextInputDialog dialog = new TextInputDialog();
	    		dialog.initOwner(stage); 
	    		dialog.setTitle("Rename Album");
	    		dialog.setHeaderText("Enter new name");
	    		Optional<String> result = dialog.showAndWait();
	    		
	    		if(result.isPresent())
	    		{
	    			if(user.getalbums().contains(new Album(result.get())))
	    			{
	    				 Alert alert = new Alert(AlertType.INFORMATION);
	 					alert.setTitle("Alert");
	 					alert.setContentText("Album of same name already exists");
	 					alert.show();
	    			}
	    			else
	    			{
	    				Album a = albumlist.getSelectionModel().getSelectedItem();
	    				int index = albumlist.getSelectionModel().getSelectedIndex();
	    				user.getAlbum(a.getname()).setname(result.get());
	    				userslist store= userslist.read();
	    				store.getuser(user.getname()).getalbums().get(index).setname(result.get());
	    				userslist.writeObject(store);
	    			}
	    			
	    		}
		 }
		
		 start(stage);
	    }
	
}
