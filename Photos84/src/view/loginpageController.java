package view;

import java.io.IOException;
import java.util.ArrayList;

import javafx.application.Application;
import Model.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
/**
 * This class houses a controller to launch a GUI that allows the user to enter a username to log in as a
 * user or admin.
 * @author Cole Stevens
 * @author Anuj Venkatesan
 * @version 1.0
 */
public class loginpageController {
	@FXML Button login;
	@FXML Button close;
	@FXML TextField Username;
	Stage stage;
	/**
	 * This method takes in a ActionEvent as a parameter. Depending on the button clicked by the user in the
	 * GUI and the username entered, the method will call the {@link #advance(Stage)} or 
	 * {@link #adminadvance(Stage)} methods.
	 * @param e the ActionEvent triggered from the user pressing a button in the GUI.
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @see #advance(Stage)
	 * @see #adminadvance(Stage)
	 */
	public void convert(ActionEvent e) throws IOException, ClassNotFoundException {
		Button b = (Button)e.getSource();
		userslist a = userslist.read();
		
		
		if(b == login)
		{

			//			Alert alert = new Alert(AlertType.INFORMATION);
//			alert.setTitle("Alert");
//			alert.setContentText("Login Successful");
//			alert.showAndWait();
	
			if(a.getusers().contains(new User(Username.getText())))
			{
		advance(stage);
			}
			else if (Username.getText().equals("admin")){
				adminadvance(stage);
			}
			else
			{
			Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Alert");
				alert.setContentText("invalid username");
				alert.showAndWait();
			}
		
		}
		else
		{
			
			System.exit(0);
		}
	
	}
	/**
	 * This method takes a Stage variable as a parameter and sets it as the <code>stage</code> variable
	 * saved locally to the controller.
	 * @param m the Stage to be saved locally to the Controller.
	 */
	public void setStage(Stage m)
	{
		stage = m;
	}
	/**
	 * This method takes in a Stage variable as a parameter. It then creates a GridPane tied to the User
	 * Controller and sets it equal to the parameter. The <code>userslist</code> saved on a local filed is
	 * called and the corresponding <code>user</code> is then found and passed to the Stage.
	 * @param primaryStage
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @see Model.userslist#userslist()
	 * @see Model.userslist#read()
	 * @see Model.userslist#getuser(String)
	 */
	private void advance(Stage primaryStage) throws IOException, ClassNotFoundException
	{
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/user.fxml"));
		GridPane root = (GridPane)loader.load();
		UserController a = loader.getController();
		userslist b = userslist.read();
		a.setStage(stage);
		a.user = b.getuser(Username.getText());
		a.searchalbum = new ArrayList<Photo>();
		a.start(stage);
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Photos");
		primaryStage.setResizable(false);
		primaryStage.show();
	}
	/** 
	 * This method takes in a Stage variable as a parameter. Then it creates a new GridPane tied to the 
	 * adminuserController and sets it equal to the parameter. 
	 * @param primaryStage
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private void adminadvance(Stage primaryStage) throws IOException, ClassNotFoundException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/adminuser.fxml"));
		GridPane root = (GridPane)loader.load();
		adminuserController a = loader.getController();
		//userslist b = userslist.read();
		a.setStage(stage);
		//a.user = b.getuser(Username.getText());
		a.start(stage);
		
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Photos");
		primaryStage.setResizable(false);
		primaryStage.show();
	}
}
