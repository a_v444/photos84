package view;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import Model.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
/**
 * This class is the controller which houses the methods to run the GUI that shows the user a slideshow of
 * photos from their album.
 * @author Cole Stevens
 * @author Anuj Venkatesan
 * @version 1.0
 *
 */
public class slideshowController {
	  @FXML
	    private Button back;

	    @FXML
	    private ImageView imageview;

	    @FXML
	    private Button logout;

	    @FXML
	    private Button next;

	    @FXML
	    private Button previous;
	    Album album;
	    User user;
	    Stage stage;
	    int index;
		
	    
	    @FXML
	    /**
	     * This method takes a Stage variable as a parameter and creates an input stream of all the 
	     * <code>Photo</code> objects in a <code>Album</code> object. The photo that appears corresponds to
	     * the local variable {@link #index}.
	     * @param stage the Stage housing the GUI
	     * @throws FileNotFoundException
	     * @see {@link FileInputStream#FileInputStream(String)}
	     * @see Image#Image(InputStream)
	     */
	    public void start(Stage stage) throws FileNotFoundException
	    {
	    	InputStream stream = new FileInputStream(album.getphotos().get(index).getfile());
	    	Image image = new Image(stream);
	    	imageview.setImage(image);
	    }
	    /**
	     * This method takes an ActionEvent as a parameter. If the user clicks the {@link #next} button, the
	     * method will push the index and trigger the #start method to display the next photo.
	     * @param event the ActionEvent triggered by a button the user pressed.
	     * @throws FileNotFoundException
	     * @see {@link Model.Album#getphotos()}
	     * @see #start(Stage) 
	     */
	    public void changephoto(ActionEvent event) throws FileNotFoundException {
	    	Button b = (Button)event.getSource();
	    	if(b == next)
	    	{
	    		if(index == album.getphotos().size()-1)
	    		{
	    			 Alert alert = new Alert(AlertType.INFORMATION);
						alert.setTitle("Alert");
						alert.setContentText("This is the last photo in the list");
						alert.show();
	    		}
	    		else
	    		{
	    			index++;
	    		}
	    	}
	    	else
	    	{
	    		if(index == 0)
	    		{
	    			Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Alert");
					alert.setContentText("This is the first photo in the list");
					alert.show();
	    		}
	    		else
	    		{
	    			index--;
	    		}
	    	}
	    	start(stage);
	    }
	    /**
		 * This method takes a Stage variable as a parameter and sets it as the <code>stage</code> variable
		 * saved locally to the controller.
		 * @param m the Stage to be saved locally to the Controller.
		 */
	    public void setStage(Stage m)
	    {
	    	stage = m;
	    }
	    /**
	     * This method takes in an ActionEvent as a parameter. Based on the button clicked, the method will
	     * either call the {@link #logout(Stage)} method or the {@link #Album(Stage)} method.
	     * @param event the ActionEvent triggered by the user when they clicked a button
	     * @throws IOException
	     * @throws ClassNotFoundException
	     * @see #logout(Stage)
	     * @see #Album(Stage)
	     */
	    @FXML
	   public void convert(ActionEvent event) throws IOException, ClassNotFoundException {
	    	
	    	Button b = (Button)event.getSource();
	    	if(b == logout)
	    	{
	    		logout(stage);
	    	}
	    	else
	    	{
	    		Album(stage);
	    	}
	    	
	    }
	    /**
	     * This method takes a Stage variable as the parameter. and creates a new Stage as a GridPane and 
	     * passes the parameter to be the newly created GridPane. This GridPane corresponds to the 
	     * album.fxml which will launch the GUI for the Album. This method also passes the 
	     * <code>User</code> and <code>Album</code> objects to the next Stage.
	     * @param primaryStage the Stage to be set as the newly created GridPane
	     * @throws ClassNotFoundException
	     * @throws IOException
	     */
	    private void Album(Stage primaryStage) throws ClassNotFoundException, IOException
	    {
	    	FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/view/album.fxml"));
			GridPane root = (GridPane)loader.load();
			Scene scene = new Scene(root);
			AlbumController a = loader.getController();
			a.setStage(primaryStage);
			a.user = user;
		
			a.album = album;
			a.start(primaryStage);
			primaryStage.setScene(scene);
			primaryStage.setTitle("Photos");
			primaryStage.setResizable(false);
			primaryStage.show();
	    }
	    /**
		 * This method takes a Stage variable as a parameter and creates a new Stage as a GridPane and passes
		 * the parameter to be the newly created GridPane. This GridPane corresponds to the loginpage.fxml which
		 * will launch the GUI for the Login page.
		 * @param primaryStage the Stage to be set as the newly created GridPane
		 * @throws IOException
		 * @see {@link #setStage(Stage)}
		 */
	    private void logout(Stage primaryStage) throws IOException
	    {
	    	FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/view/loginpage.fxml"));
			GridPane root = (GridPane)loader.load();
			Scene scene = new Scene(root);
			loginpageController a = loader.getController();
			a.setStage(primaryStage);

			primaryStage.setScene(scene);
			primaryStage.setTitle("Photos");
			primaryStage.setResizable(false);
			primaryStage.show();
	    }

}
