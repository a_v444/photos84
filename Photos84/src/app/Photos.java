package app;

import Model.Photo;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import view.loginpageController;

/**
 * This class houses the application that creates the <code>primaryStage</code> and initializes it to
 * a GridPane of the login page. It is the first thing that loads when running the code. This class
 * extends the Application class.
 * @author Cole Stevens
 * @author Anuj Venkatesan
 * @version 1.0
 * @see Photos{@link #start(Stage)}
 * @see view.loginpageController
 *
 */
public class Photos extends Application {
	
	/**
	 * This is the first method that is called when the application starts. This overrides the main method
	 * and launches the <code>primaryStage</code> to be a GridPane of the login page as stated previously.
	 * @param primaryStage the main Stage which will house each of our GUIs to run the application. 
	 */
	@Override
	public void start(Stage primaryStage) 
	throws Exception {
		// TODO Auto-generated method stub
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/loginpage.fxml"));
		GridPane root = (GridPane)loader.load();
		Scene scene = new Scene(root);
		loginpageController a = loader.getController();
		a.setStage(primaryStage);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Photos");
		primaryStage.setResizable(false);
		primaryStage.show();
		
		
		
	}
	/**
	 * This method launches <code>args</code> which in turn calls the start method to create the first stage.
	 * @param args this 1D String array is not filled from the console. Used to call {@link #start(Stage)}.
	 * @see package.Photos{@link #start(Stage)}
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);

	}

}
