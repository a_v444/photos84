package Model;

import java.util.ArrayList;
import java.io.*;
import java.util.Date;
/**
 * This class is used to create and alter an <code>Album</code> instance variable. Which can save multiple 
 * <code>Photo</code> instance variables, as well as the earliest and latest <code>Date</code> that a 
 * <code>Photo</code> was added to the album. 
 * @author Cole Stevens
 * @author Anuj Venkatesan
 * @version 1.0
 * @see Model.Photo#Photo(String)
 * @see Date#Date()
 *
 */
public class Album implements Serializable {
	ArrayList<Photo> photos;
	String name;
	Date earliest;
	Date latest;
	static final long serialVersionUID = 1L;
	
	/**
	 * This method initializes an object instance variable of <code>Album</code>. Takes in a
	 * String parameter as the name and initializes a blank ArrayList of type
	 * <code>Photo</code>.
	 * @param n A String to name the <code>Album</code>
	 * @see Model.Photo#Photo(String)
	 */
	public Album(String n)
	{
		name = n;
		photos = new ArrayList<Photo>();
	}
	/**
	 * This method returns an ArrayList of type <code>Photo</code> for a <code>Album</code> 
	 * instance.
	 * @return An ArrayList of <code>Photo</code> for an specific <code>Album</code> instance.
	 * @see Model.Photo#Photo(String)
	 * @see #Album(String)
	 */
	public ArrayList<Photo> getphotos()
	{
		return photos;
	}
	/**
	 * This method returns a String representing the name of a <code>Album</code> instance.
	 * @return A String of a <code>Album</code> instance representing the name.
	 * @see #Album(String)
	 */
	public String getname()
	{
		return name;
	}
	/**
	 * This method returns a boolean which represents if two <code>Album</code> names are the same or 
	 * different.
	 * @return a boolean representing if two <code>Album</code> names are the same or different
	 * @see #Album(String)
	 */
	public boolean equals(Object o)
	{
		if(o == null)
		{
			return false;
		}
		return name.equals(((Album) (o)).getname());
		
	}
	/**
	 * This method takes in a <code>Photo</code> parameter and checks to see if it is contained inside
	 * the <code>Album</code> instance variable.
	 * @param b A <code>Photo</code> which we are searching for.
	 * @return A <code>Photo</code> from inside the album that we were searching for.
	 * @see #Album(String)
	 * @see Model.Photo#Photo(String)
	 */
	public Photo getPhoto(Photo b)
	{
		for(Photo a:photos)
		{
			if(a.equals(b))
			{
				return a;
			}
		}
		return null;
	}
	/**
	 * This method searches through all the <code>Photo</code> Objects inside an <code>Album</code> Object
	 * and checks all the <code>Date</code> values and returns the earliest one.
	 * @return The <code>Date</code> the first <code>Photo</code> was added to the <code>Album</code> Object.
	 * @see #Album(String)
	 * @see Model.Photo#Photo(String)
	 * @see Date#Date()
	 */
	public Date getearliest()
	{
		if(photos.isEmpty())
		{
			return null;
		}
		Date min = photos.get(0).date;
		for(int i =0; i < photos.size();i++)
		{
			if(photos.get(i).date.before(min))
			{
				min = photos.get(i).date;
			}
		}
		return min;
	}
	/**
	 * This method searches through all the <code>Photo</code> Objects inside an <code>Album</code> Object
	 * and checks all the <code>Date</code> values and returns the latest one.
	 * @return The <code>Date</code> the last <code>Photo</code> was added to the <code>Album</code> Object.
	 * @see #Album(String)
	 * @see Model.Photo#Photo(String)
	 * @see Date#Date()
	 */
	public Date getlatest()
	{
		if(photos.isEmpty())
		{
			return null;
		}
		Date max = photos.get(0).date;
		for(int i =0; i < photos.size();i++)
		{
			if(photos.get(i).date.after(max))
			{
				max = photos.get(i).date;
			}
		}
		return max;
	}
	/**
	 * This method adds a <code>Photo</code> instance variable to an ArrayList of type <code>Photo</code>
	 * contained inside an <code>Album</code> object.
	 * @param a The <code>Photo</code> object to be added into the ArrayList of the same type inside the <code>Album</code> object.
	 * @see #Album(String)
	 * @see Model.Photo#Photo(String)
	 */
	public void addPhoto(Photo a)
	{
		photos.add(a);
		}
	/**
	 * This method deletes a <code>Photo</code> instance variable from an ArrayList of type <code>Photo</code>
	 * contained inside an <code>Album</code> object.
	 * @param a The <code>Photo</code> object to be deleted in the ArrayList of the same type inside the <code>Album</code> object.
	 * @see #Album(String)
	 * @see Model.Photo#Photo(String)
	 */
	public void deletePhoto(Photo b)
	{
		photos.remove(b);
	}
	/**
	 * This method changes the name of an <code>Album</code> instance variable.
	 * @param n The String to represent the new name of the <code>Album</code>.
	 * @see #Album(String)
	 */
	public void setname(String n)
	{
		name = n;
	}
	/**
	 * This method returns a String representing various information about an <code>Album</code> instance
	 * variable. Specifically, the name, number of <code>Photo</code> objects saved in the <code>Album</code>
	 * object, and the <code>Date</code> of the earliest and latest <code>Photo</code> objects added.
	 * @return A String representing various information about an <code>Album</code> instance variable.
	 * @see #Album(String)
	 * @see Model.Photo#Photo(String)
	 * @see Date#Date()
	 */
	public String toString()
	{
		return name + "   " + photos.size() + "   " + getearliest() + " to " + getlatest();
	}
}

