package Model;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
/**
 * A class that houses methods that can create and alter a <code>Photo</code> object. Storing information such as the
 * location of a local photo file, a caption, photo tags, and the date the photo was taken. 
 * @author Cole Stevens
 * @author Anuj Venkatesan
 * @version 1.0
 * @see #Photo(String)
 * @see Model.Tag#Tag(String)
 */
public class Photo implements Serializable {
Date date;
String caption;
ArrayList<Tag> tags;
String file;
static final long serialVersionUID = 1L;
/**
 * This method takes in a String argument which represents the location of a local photo file
 * and creates a <code>Photo</code> instance variable. Storing the String parameter and creating and storing
 * a new ArrayList of type <code>Tag</code> inside.
 * @param f a String argument which represents the location of a local photo file
 * @see Model.Tag#Tag(String)
 */
public Photo(String f)
{
	file = f;
	tags = new ArrayList<Tag>();
	tags.add(new Tag("location"));
	tags.add(new Tag("person"));
	
}
/**
 * This method takes in a String argument which represents the location of a local photo file and changes
 * the variable <code>file</code> inside a <code>Photo</code> Object to be the string parameter.
 * @param s the String representing the location of the local photo file.
 * @see #Photo(String)
 */
public void setfile(String s)
{
	file = s;
}
/** 
 * This method takes in an Object (which will only be called for type <code>Photo</code>) and returns a
 * boolean representing whether or not two photos are the same by checking if they are stored to the same
 * location.
 * @param An Object (which will only be called for type <code>Photo</code>)
 * @return A boolean representing if two <code>Photo</code> objects are stored to the same place locally
 * @see #Photo(String)
 */
public boolean equals(Object o)
{
	if(o == null)
	{
		return false;
	}
	return file.equals(((Photo) o).getfile());
}
/**
 * This method takes in a String representing the caption and stores it inside a <code>Photo</code> object.
 * @param c the String representing the caption of a <code>Photo</code> object.
 * @see #Photo(String)
 */
public void setcaption(String c)
{
	caption = c;
}
/**
 * This method takes in a <code>Date</code> object and stores it inside a <code>Photo</code> object.
 * @param c the <code>Date</code> representing the date of a <code>Photo</code> object.
 * @see #Photo(String)
 * @see Date#Date()
 */
public void setDate()
{
	
	File a = new File(file);
	
	date = new Date(a.lastModified());
	Calendar time = Calendar.getInstance();
	time.setTime(date);
	time.set(Calendar.MILLISECOND,0);
	date = time.getTime();
}
/**
 * This method returns a String representing the caption that is stored inside a <code>Photo</code> object.
 * @return the String representing the caption of a <code>Photo</code> object.
 * @see #Photo(String)
 */
public String getcaption()
{
	return caption;
}
/**
 * This method returns the <code>Date</code> object representing the date that is stored inside a
 * <code>Photo</code> object.
 * @return the <code>Date</code> object representing the date of a <code>Photo</code> object.
 * @see #Photo(String)
 * @see Date#Date()
 */
public Date getdate()
{
	return date;
}
/**
 * This method returns a String representing the location of a photo file that is stored locally which
 * is stored inside a <code>Photo</code> object.
 * @return the String representing the location of a photo file that is stored locally.
 * @see #Photo(String)
 */
public String getfile()
{
	return file;
}
/**
 * This method returns the ArrayList of type <code>Tag</code> that is stored inside a <code>Photo</code> 
 * object.
 * @return the ArrayList of type <code>Tag</code>.
 * @see #Photo(String)
 * @see Model.Tag#Tag(String)
 */
public ArrayList<Tag> gettags()
{
	return tags;
}
/**
 * This method adds a <code>Tag</code> to the ArrayList of type <code>Tag</code> that is stored inside a 
 * <code>Photo</code> object.
 * @see #Photo(String)
 * @see Model.Tag#Tag(String)
 */
public void addtag(Tag t)
{
	if(!tags.contains(t))
	{
	tags.add(t);
	}
}
/**
 * This method takes in a String parameter representing a <code>Tag</code> and returns a matching 
 * <code>Tag</code> that is stored inside a <code>Photo</code> object.
 * @param t the String parameter representing a <code>Tag</code> inside a <code>Photo</code> object.
 * @return the matching <code>Tag</code> given by the parameter.
 * @see #Photo(String)
 * @see Model.Tag#Tag(String)
 */
public Tag gettag(String t)
{
	for(int i =0; i < tags.size(); i++)
	{
		if(tags.get(i).type.equals(t))
		{
			return tags.get(i);
		}
	}
	return null;
}
/**
 * This method takes in a <code>Tag</code> object as a parameter and deletes a matching <code>Tag</code>
 * from an ArrayList of a matching type stored inside a <code>Photo</code> object.
 * @param t a <code>Tag</code> object to be deleted from an ArrayList inside a <code>Photo</code> object.
 * @see #Photo(String)
 * @see Model.Tag#Tag(String)
 */
public void deletetag(Tag t)
{
	tags.remove(t);
}
/**
 * This method returns a String which gives various information about a <code>Photo</code> object. Such as
 * the captions, all the <code>Tag</code> objects, and date of the <code>Photo</code> object.
 * @return a String which gives various information about a <code>Photo</code> object.
 * @see #Photo(String)
 * @see Model.Tag#Tag(String)
 */
public String toString()
{
	String result = "Caption: " + caption + "\nDate: " + date + "\ntags:";
	for(Tag a: tags)
	{
		
		result += "\n" + a;
	}
	return result;
}
}
