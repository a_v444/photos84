package Model;

import java.util.ArrayList;
import java.io.*;
/**
 * This class houses methods used to create and alter a <code>User</code> instance variable. 
 * @author Cole Stevens
 * @author Anuj Venkatesan
 * @version 1.0
 *
 */
public class User implements Serializable{

	ArrayList<Album> albums;
	String username;
	static final long serialVersionUID = 1L;

	


	/**
	 * This method takes in a String parameter and creates a <code>User</code> instance variable. The 
	 * parameter is stored as a String variable <code>username</code> and a blank ArrayList of type
	 * <code>Album</code> is created.
	 * @param u
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @see Model.Album#Album(String)
	 * @see #username
	 */

	public User(String u) throws ClassNotFoundException, IOException
	{
		
		
		username = u;
		albums = new ArrayList<Album>();
	
		
	}
	/**
	 * This method takes in a Object parameter (will be of type <code>User</code>) and checks to see if the
	 * usernames stored inside a <code>User</code> object are the same. 
	 * @param an Object (will be of type <code>User</code>)
	 * @return a boolean representing whether or not these two Objects are the same
	 * @see #User(String)
	 * @see #getname()
	 * @see #username
	 */
	public boolean equals(Object o)
	{
		if(o == null)
		{
			return false;
		}
	

		return username.equals(((User) (o)).getname());
	}
	/**
	 * This method returns an ArrayList of type <code>Album</code> stored inside a <code>User</code> object.
	 * @return an ArrayList of type <code>Album</code>
	 * @see #User(String)
	 * @see Model.Album#Album(String)
	 */
	public ArrayList<Album> getalbums()
	{
		return albums;
	}
	/**
	 * This method returns a String which represents the username. This is stored inside a 
	 * <code>User</code> object as a String variable.
	 * @return a String representing the username of the code>User</code> object.
	 * @see #User(String)
	 * @see #username
	 */
	public String getname()
	{
		return username;
	}
	/**
	 * This method adds an <code>Album</code> object to an ArrayList of the same type stored inside a
	 * <code>User</code> object. The <code>User</code> object is obtained from a <code>userlist</code> object
	 * and after altering the <code>User</code> object the changes are saved on a local file.
	 * @param a an <code>Album</code> object to be stored inside the <code>User</code> object.
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @see #User(String)
	 * @see Model.Album#Album(String)
	 * @see Model.userslist#userslist()
	 * @see Model.userslist#writeObject(userslist)
	 */
	public void addAlbum(Album a) throws ClassNotFoundException, IOException
	{
		albums.add(a);
		
		userslist list = userslist.read();
		list.getuser(username).albums.add(a);
		userslist.writeObject(list);

	}
	
	/**
	 * This method takes in a String parameter that represents the name of an <code>Album</code> object and 
	 * searches for it in a <code>User</code> object.
	 * @param name a String that represents the name of an <code>Album</code> object
	 * @return an <code>Album</code> object with the same <code>name</code> variable as the parameter.
	 * @see Model.Album#Album(String)
	 */

	public Album getAlbum(String name)
	{
		for(int i = 0; i < albums.size();i++)
		{
			if(albums.get(i).getname().equals(name))
			{
				return albums.get(i);
			}
		}
		return null;
	}
	/**
	 * This method takes in an <code>Album</code> object as the parameter and searches for it inside a 
	 * <code>User</code> object. Then saves the changes on a local file.
	 * @param b the <code>Album</code> object to be deleted.
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @see #User(String)
	 * @see Model.Album#Album(String)
	 * @see Model.userslist#userslist()
	 * @see Model.userslist#read()
	 * @see Model.userslist#getuser(String)
	 * @see Model.userslist#writeObject(userslist)
	 */
	public void deleteAlbum(Album b) throws IOException, ClassNotFoundException
	{
		albums.remove(b);
	
		userslist list = userslist.read();
		list.getuser(username).albums.remove(b);
		userslist.writeObject(list);
		
		
	}
	/**
	 * This method returns the username saved in a <code>User</code> object as a String variable.
	 * @return a String representing the username of a <code>User</code> object
	 * @see #username
	 * @see #User(String)
	 */
	public String toString()
	{
		return username;
	}
}

