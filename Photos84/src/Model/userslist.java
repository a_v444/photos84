package Model;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
/**
 * This class houses methods which are used to create a <code>userslist</code> object, 
 * manipulate it, and save the changes to a local file.
 * @author Cole Stevens
 * @author Anuj Venkatesan
 * @version 1.0
 *
 */
public class userslist implements Serializable {
	ArrayList<User> users;
	public static final String storeDir = "src";
	public static final String storeFile = "users.dat";
	static final long serialVersionUID = 1L;
	/**
	 * This method creates a <code>userslist</code> object. Stores an empty ArrayList of type 
	 * <code>User</code> inside.
	 * @throws IOException
	 * @see {@link Model.User#User(String)}
	 */
	public userslist() throws IOException
	{
		users = new ArrayList<User>();
		
		}
	/**
	 * This method saves a <code>userslist</code> object to a local file and returns it.
	 * @return a <code>userslist</code> object that was saved to a local file.
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @see #User(String)
	 * @see Model.Album#Album(String)
	 * @see Model.userslist#userslist()
	 * @see #writeObject(userslist)
	 */
	public static userslist read() throws IOException, ClassNotFoundException
	{
		try {
		ObjectInputStream ois = new ObjectInputStream(
				new FileInputStream(storeDir + File.separator + storeFile));
		userslist one = (userslist)ois.readObject();
		return one;
		}
		catch (EOFException e) {
		
		}
		userslist one = new userslist();
		one.adduser(new User("stock"));
		return one;
	
	}
	/**
	 * This method returns the ArrayList of type <code>User</code> from a <code>userslist</code> object.
	 * @return an ArrayList of all the <code>User</code> objects
	 * @see Model.User#User(String)
	 */
	public ArrayList<User> getusers()
	{
		return users;
	}
	/**
	 * This method takes in a String representing the <code>username</code> saved in a <code>User</code>
	 * object and finds that object.
	 * @param username a String representing the <code>username</code> saved in a <code>User</code> object
	 * @return a <code>User</code> object with a matching <code>username</code> from the parameter.
	 * @see Model.User#User(String)
	 * @see Model.User#equals(Object)
	 */
	public User getuser(String username)
	{
		for(int i =0; i < users.size(); i++)
		{
			if(username.equals(users.get(i).username))
			{
				return users.get(i);
			}
		}
		return null;
	}
	/**
	 * This method takes in a <code>User</code> object and adds it to the <code>userslist</code> object 
	 * saved on a local file.
	 * @param u a <code>User</code> object to be saved to the local file of the <code>userslist</code object
	 * @throws IOException
	 * @see #User(String)
	 * @see Model.userslist#userslist()
	 * @see #writeObject(userslist)
	 * 
	 */
	public void adduser(User u) throws IOException
	{
		if(!users.contains(u))
		{
		users.add(u);
		writeObject(this);
		
		
		}
	}
	/**
	 * This method takes in a <code>userslist</code> object and saves it to a local file. 
	 * @param a a <code>userslist</code> object to be saved to a local file. 
	 * @throws IOException
	 * @see {@link #writeObject(userslist)}
	 * @see ObjectOutputStream#ObjectOutputStream(OutputStream)
	 * @see FileOutputStream#FileOutputStream(File)
	 */
	public static void writeObject(userslist a) throws IOException
	{
		ObjectOutputStream oos = new ObjectOutputStream(
				new FileOutputStream(storeDir + File.separator + storeFile));
		oos.writeObject(a);
	}
	/**
	 * This method takes in a <code>User</code> object and deletes it from a <code>userslist</code> object
	 * and saves the changes on a local file.
	 * @param u
	 * @throws IOException
	 * @see {@link #writeObject(userslist)}
	 * @see Model.User#User(String)
	 * @see #userslist()
	 */
	public void deleteuser(User u) throws IOException
	{
		users.remove(u);
		writeObject(this);
	}
}
