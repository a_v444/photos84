package Model;

import java.io.Serializable;
import java.util.ArrayList;
/**
 * This class houses methods that can create and alter a <code>Tag</code> instance variable.
 * @author Cole Stevens
 * @author Anuj Venkatesan
 * @version 1.0 *
 */
public class Tag implements Serializable {
	String type;
	ArrayList<String> values;
	static final long serialVersionUID = 1L;
	/**
	 * This method takes in a String parameter which represents the <code>type</code> of the Tag (if it is
	 * a tag of people in the photo or where the photo was taken). Also creates and stores a blank ArrayList of type
	 * String to store all the tags of that type.
	 * @param t the String to represent the type of the tags stored in the <code>Tag</code> variable.
	 */
	public Tag(String t)
	{
		type = t;
		values = new ArrayList<String>();
		
	}
	/**
	 * This method takes in a String parameter which represents the tag itself (the person or location of the
	 * photo).
	 * @param v the String representing the tag itself (the person or location of the photo)
	 * @see #Tag(String)
	 */
	public void addvalue(String v)
	{
		if(!values.contains(v))
		{
			values.add(v);
		}
	}
	/**
	 * This method returns the type of tag a <code>Tag</code> object is. Which is stored in a String
	 * variable inside the object called <code>type</code>
	 * @return a String variable representing the type of the <code>Tag</code> object
	 * @see #Tag(String)
	 */
	public String gettype()
	{
		return type;
	}
	/**
	 * This method returns the tags housed in a ArrayList of type String inside a <code>Tag</code> object. 
	 * @return the tags inside an ArrayList of type String inside a <code>Tag</code> object.
	 * @see #Tag(String)
	 */
	public ArrayList<String> getvalues()
	{
		return values;
	}
	/**
	 * This method removes a tags housed in a ArrayList of type String inside a <code>Tag</code> object. 
	 * @param the tag to be removed from inside an ArrayList of type String in a <code>Tag</code> object.
	 * @see #Tag(String)
	 */
	public void removevalue(String v)
	{
	values.remove(v);
	}
	/**
	 * This method checks if two Objects (will only be called for type <code>Tag</code>) have the same type.
	 * The type is stored in a String variable inside the <code>Tag</code> object called <code>type</code>.
	 * @param the <code>Tag</code> object whose type we are comparing.
	 * @return a boolean representing if two <code>Tag</code> objects share the same type.
	 * @see #Tag(String)
	 */
	public boolean equals(Object o)
	{
		if(o == null)
		{
			return false;
		}
		return type.equals((((Tag) o).gettype()));
	}
	/**
	 * This method returns a String representing the type of tag and the tags of a <code>Tag</code> object.
	 * @return the String representing various information about a <code>Tag</code> object.
	 * @see #Tag(String)
	 */
	public String toString()
	{
		String result = type + "=";
		
		for(String i: values)
		{
			
			result += i + ",";
		}
		if(result.substring(result.length()-1).equals(","))
		{
			return result.substring(0,result.length()-1);
		}
		return result;
	}
}
